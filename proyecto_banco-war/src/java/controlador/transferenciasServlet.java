
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Login;
import modelo.Cliente;
import modelo.Transferencia;
import modelo.Cuenta;
import modelo.Historicotrans;

public class transferenciasServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
        String rutDestinatario = request.getParameter("rutDestinatario");
        String nombreDestinatario = request.getParameter("nombreDestinatario");
        int monto = Integer.parseInt(request.getParameter("monto"));
        String clave = request.getParameter("clave");
        
        //verificacion de clave
        Login verificacion = new Login();
        Cliente cliente = (Cliente) request.getSession().getAttribute("cliente");   
        
        if (verificacion.autenticacion(cliente.getRut(), clave)){
            
            int saldoOrigen, saldoDestino, idTransOrigen, idTransDestino;
            String descripcionOrigen, descripcionDestino;
            //verifica cuenta destino
            Cliente destinatario = new Cliente();
            
            if (destinatario.buscaCliente(rutDestinatario, nombreDestinatario)){
                
                //inserta en tabla transferencia
                Transferencia transferencia = new Transferencia();
                transferencia.insertar(cliente.getRut());
                transferencia.insertar(destinatario.getRut());
                
                //obtiene los id's de cada transferencia
                idTransOrigen = transferencia.getIdTrans(cliente.getRut());
                idTransDestino = transferencia.getIdTrans(destinatario.getRut());
                
                //obtiene los saldos de cada cuenta
                Cuenta cuentaCliente = new Cuenta();
                saldoOrigen= cuentaCliente.getSaldo(cliente.getRut());
                saldoDestino = cuentaCliente.getSaldo(destinatario.getRut());
                
                //resta el monto en el saldo origen y lo suma en el saldo destino
                cuentaCliente.setSaldo(cliente.getRut(), saldoOrigen -= monto);
                cuentaCliente.setSaldo(destinatario.getRut(), saldoDestino += monto);
                
                //inserta la transaccion en el historico
                Historicotrans historico = new Historicotrans();
                descripcionOrigen = "Transferencia hacia " + destinatario.getRut();
                descripcionDestino = "Transferencia desde " + cliente.getRut();
                historico.insert(idTransOrigen, descripcionOrigen, monto, saldoOrigen);
                historico.insert(idTransDestino, descripcionDestino, monto, saldoDestino);
                
                response.sendRedirect("transferenciaExitosa.jsp");                
            }  
        }
        response.sendRedirect("transferenciaError.jsp");  
    }       
        
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            processRequest(request, response);
        }catch(Exception ex){
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
            processRequest(request, response);
        }catch(Exception ex){
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

