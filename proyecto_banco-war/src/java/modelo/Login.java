
package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Login extends Conexion {
    
    private String rut;
    private String clave;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    public boolean autenticacion(String rut, String clave) throws SQLException{ 
    
        Statement st = con.createStatement();
        ResultSet rs = null;
        String consulta = "Select * from tbl_login";
    
        rs = st.executeQuery(consulta);
    
        while(rs.next()){
            
            if(rut.equals(rs.getString("rut")) && clave.equals(rs.getString("clave"))){
                
                this.rut = rut;
                return true;
            }
        }
    return false;
    }
}   
