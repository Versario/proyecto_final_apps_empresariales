
package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Cuenta extends Conexion{
    
    private String rut;
    private String ncuenta;
    private int saldot;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNcuenta() {
        return ncuenta;
    }

    public void setNcuenta(String ncuenta) {
        this.ncuenta = ncuenta;
    }

    public int getSaldot() {
        return saldot;
    }

    public void setSaldot(int saldot) {
        this.saldot = saldot;
    }
    
        public int getSaldo(String rut) throws SQLException{
    
        String consulta = "Select * from tbl_cuenta where rut = ?";
        
        try{
            
            PreparedStatement pst = con.prepareStatement(consulta);
            pst.setString(1, rut);
            ResultSet rs = pst.executeQuery();
        
            rs.next();
            this.rut = rut;
            this.ncuenta = rs.getString("ncuenta");
            this.saldot = rs.getInt("saldot");
            
                            
        }catch(Exception e){
            System.out.println("Error al leer" + e.toString());
        }
        return this.saldot;
    }
        
        public void setSaldo(String rut, int saldo) throws SQLException{
        
            String consulta = "UPDATE tbl_cuenta SET saldot=? where rut = ?";
            
            try{
                
                PreparedStatement pst = con.prepareStatement(consulta);
                pst.setInt(1, saldo);
                pst.setString(2, rut);
                pst.execute();
                //nuevo saldo
                this.saldot = saldo;
                
            }catch(Exception e){
                System.out.println("Error al actualizar " + e.toString());
            }
        }
}
