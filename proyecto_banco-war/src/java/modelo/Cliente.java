
package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Cliente extends Conexion {
    
    private String rut;
    private String nombre;
    private String apellidopat;
    private String apellidomat;
    private String email;
    private int fono;

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidopat() {
        return apellidopat;
    }

    public void setApellidopat(String apellidopat) {
        this.apellidopat = apellidopat;
    }

    public String getApellidomat() {
        return apellidomat;
    }

    public void setApellidomat(String apellidomat) {
        this.apellidomat = apellidomat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getFono() {
        return fono;
    }

    public void setFono(int fono) {
        this.fono = fono;
    }
    
    public void datos(String rut) throws SQLException{
    
        String consulta = "Select * from tbl_cliente where rut = ?";
        
        try{
            
            PreparedStatement pst = con.prepareStatement(consulta);
            pst.setString(1, rut);
            ResultSet rs = pst.executeQuery();
        
            rs.next();
            this.rut = rut;
            this.nombre = rs.getString("nombre");
            this.apellidopat = rs.getString("apellidopat");
            this.apellidomat = rs.getString("apellidomat");
            this.email = rs.getString("email");
            this.fono = rs.getInt("fono");
                            
        }catch(Exception e){
            System.out.println("Error al leer" + e.toString());
        }
    }
    
    public boolean buscaCliente(String rut, String nombre) throws SQLException{
    
        String consulta = "Select * from tbl_cliente";
        
        try{
            
            Statement st = con.createStatement();
            ResultSet rs = null;
    
            rs = st.executeQuery(consulta);
        
            while(rs.next()){
                
                if(rut.equals(rs.getString("rut")) && nombre.equals(rs.getString("nombre"))){
                    
                    this.rut = rut;
                    this.nombre = nombre;
                    this.apellidopat = rs.getString("apellidopat");
                    this.apellidomat = rs.getString("apellidomat");
                    this.email = rs.getString("email");
                    this.fono = rs.getInt("fono");
                    return true;
                }
            }                
        }catch(Exception e){
            System.out.println("Error al leer" + e.toString());
        }
        
        return false;
    }
}
    

