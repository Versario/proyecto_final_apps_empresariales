
package modelo;

public class Tipotrans {
    
    private int idtipotrans;
    private String nombretrans;

    public int getIdtipotrans() {
        return idtipotrans;
    }

    public void setIdtipotrans(int idtipotrans) {
        this.idtipotrans = idtipotrans;
    }

    public String getNombretrans() {
        return nombretrans;
    }

    public void setNombretrans(String nombretrans) {
        this.nombretrans = nombretrans;
    }
}
