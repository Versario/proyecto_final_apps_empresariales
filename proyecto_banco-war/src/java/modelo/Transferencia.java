
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transferencia extends Conexion{
    
    private int idtrans;
    private String rut;
    private int idtipotrans;

    public int getIdtrans() {
        return idtrans;
    }

    public void setIdtrans(int idtrans) {
        this.idtrans = idtrans;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getIdtipotrans() {
        return idtipotrans;
    }

    public void setIdtipotrans(int idtipotrans) {
        this.idtipotrans = idtipotrans;
    }
    
    public void insertar(String rut){
        
        String sql = "INSERT INTO tbl_trans(rut, idtipotrans) values (?,?)";
        
        try{
            
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, rut);
            pst.setInt(2, 1); //el valor 1 corresponde al tipo Transferencias
            pst.execute();
            
        }catch(Exception e){
            System.out.println("Error al insertar" + e.toString());
        }
    }
    
    public int getIdTrans(String rut) throws SQLException{
    
        String consulta = "SELECT MAX(idtrans) FROM tbl_trans where rut=?";
        
        try{
            
            PreparedStatement pst = con.prepareStatement(consulta);
            pst.setString(1, rut);
            ResultSet rs = pst.executeQuery();
        
            rs.next();
            this.idtrans = rs.getInt("MAX(idtrans)");
            
                            
        }catch(Exception e){
            System.out.println("Error al leer" + e.toString());
        }
        return this.idtrans;
    }
    
}
