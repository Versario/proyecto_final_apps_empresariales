package modelo;

import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Historicotrans extends Conexion{
    
    private int idtrans;
    private Date fecha;
    private String hora;
    private String descripcion;
    private int monto;
    private int saldop;

    public int getIdtrans() {
        return idtrans;
    }

    public void setIdtrans(int idtrans) {
        this.idtrans = idtrans;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public int getSaldop() {
        return saldop;
    }

    public void setSaldop(int saldop) {
        this.saldop = saldop;
    }
    
    public void insert(int idTransferencia, String descripcion, int monto, int saldo){
        
        String sql = "INSERT INTO tbl_historicotrans(idtrans, fecha, hora, descripcion, monto, saldop)"
                + " values (?,?,?,?,?,?)";
        
        java.util.Date utilDate = new java.util.Date();
        java.sql.Time sqlTime = new java.sql.Time(utilDate.getTime());
        Calendar calendar = Calendar.getInstance();
        java.sql.Date sqlDate = new java.sql.Date(calendar.getTime().getTime());
        
        try{
            
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, idTransferencia); 
            pst.setDate(2, sqlDate);
            pst.setTime(3, sqlTime);
            pst.setString(4, descripcion);
            pst.setInt(5, monto);
            pst.setInt(6, saldo);
            pst.execute();
            
        }catch(Exception e){
            System.out.println("Error al insertar" + e.toString());
        }
    }
     
    public List<Historicotrans> mostrar(String rut){
        
        String sql = "SELECT * FROM tbl_historicotrans H, tbl_trans T WHERE H.idtrans = T.idtrans and T.rut = ?"
                + "and EXTRACT(MONTH FROM fecha) = MONTH(NOW()) ORDER BY fecha ASC";
        List<Historicotrans> resultado = new ArrayList<>();
        
        try{
            
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, rut);
            ResultSet rs = pst.executeQuery();
            
            Historicotrans registro;
            
            while (rs.next()) {
                registro = new Historicotrans();
                registro.setFecha(rs.getDate("fecha"));
                registro.setMonto(rs.getInt("monto"));
                registro.setSaldop(rs.getInt("saldop"));
                registro.setDescripcion(rs.getString("descripcion"));
                
                resultado.add(registro);
            }
        }catch(Exception e){
            System.out.println("Error al mostrar" + e.toString());
        }
        
        return resultado;
    }
}
