<%-- 
    Document   : menu
    Created on : 20-11-2016, 05:42:07 PM
    Author     : pablo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.Cliente"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles/base.css" text="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu</title>
    </head>
    <body>
        <%  response.setHeader("Cache-control", "no-store");
            session.setMaxInactiveInterval(60); %>
            
        <h2>Bienvenido ${sessionScope.cliente.getNombre()} ${sessionScope.cliente.getApellidopat()}</h2>
        <center>
            <div id="menu">
            <h1>Menu</h1>
            <br>    
            <a href="consultaCuentaServlet">Consulta datos cuenta</a>
            <br>
            <a href="transferencia.jsp">Transferencias</a>
            <br>
            <br>
            <a href="cerrarSesionServlet">Cerrar Sesion</a>
            </div>
        </center>
    </body>
</html>
