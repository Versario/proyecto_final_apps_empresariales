<%-- 
    Document   : transferencia
    Created on : 20-11-2016, 06:28:44 PM
    Author     : pablo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles/base.css" text="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Transferencia</title>
    </head>
    <body>
        <%  response.setHeader("Cache-control", "no-store");
        session.setMaxInactiveInterval(60); %>
    <center>
        <div id="transferencia">
        <h1>Transferencia</h1>
        <br>
        <br>
        <form method="post" action="transferenciasServlet">
            <label> Rut Destinatario </label>
            <br>
            <input type="text" name="rutDestinatario" required>
            <br>
            <label> Nombre Destinatario </label>
            <br>
            <input type="text" name="nombreDestinatario" required>
            <br>
            <label> Monto a transferir </label>
            <br>
            <input type="number" name="monto" required>
            <br>
            <label> Clave </label>
            <br>
            <input type="password" name="clave" required>
            <br>
            <br>
            <input type="submit" name="Enviar" value="Enviar">
        </form>
        </div>
        <br>
        <br>
        <a id="volver" href="menu.jsp">Volver</a>
    </center>    
    </body> 
</html>
