<%-- 
    Document   : login
    Created on : 20-11-2016, 05:24:58 PM
    Author     : pablo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles/base.css" text="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <center>
            <div id="login">
            <h1 class="title" id="title">Login</h1>
            <br>
            <br>
            <form method="post" action="LoginServlet">
                    <label>Usuario</label>
                    <br>
                    <input type="text" name="rut" required>
                    <br>
                    <br>
                    <label>Contraseña</label>
                    <br>
                    <input type="password" name="clave" required>
                    <br>
                    <br>
                    <input class="btn" type="submit" name="Enviar" value="Enviar">
            </form>
            </div>
        </center> 
    </body>
</html>
