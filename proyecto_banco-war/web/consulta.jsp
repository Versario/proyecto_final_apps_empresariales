<%-- 
    Document   : consulta
    Created on : 20-11-2016, 06:35:44 PM
    Author     : pablo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.Historicotrans"%>
<%@page import="java.util.List"%>
<%@page import="java.util.StringTokenizer" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/base.css" text="text/css">
        <title>Consulta datos cuenta</title>
    </head>
    <body>
        <%  response.setHeader("Cache-control", "no-store");
        session.setMaxInactiveInterval(60); 
        %>
    <center>
        <h1>Consulta datos cuenta</h1>
        <br>
        <br>
        <table id="table">
            <tr>
                <th>Fecha</th>
                <th>Abono</th>
                <th>Cargo</th> 
                <th>Descripcion</th>
                <th>Saldo</th>
            </tr>
            <%
            StringTokenizer st1;  
            StringTokenizer st2;
            Historicotrans historico = new Historicotrans();
            List<Historicotrans> lista = (List<Historicotrans>) request.getAttribute("lista");
            
            for(Historicotrans registro : lista) {
                st1 = new StringTokenizer(registro.getDescripcion(), " ");
                st2 = new StringTokenizer(registro.getDescripcion(), " ");
                st1.nextToken();
                st2.nextToken();
            %>
            <tr>
                <td><label><%= registro.getFecha() %></label></td>
                <td>
                    <% if (st1.nextToken().equals("desde")){%> 
                        <label>$ <%= registro.getMonto() %></label>
                    <% }else{ %>
                        <label> </label>
                    <% } %>
                </td>           
                <td>
                    <% if (st2.nextToken().equals("hacia")){%> 
                        <label>$ <%= registro.getMonto() %></label>
                    <% }else{ %>
                        <label> </label>
                    <% } %>
                </td>
                <td><label> <%= registro.getDescripcion() %> </label></td>
                <td><label> $ <%= registro.getSaldop() %> </label></td>
            <%
            }
            %>
            </tr>
        </table>
        <br>
        <br>
        <a id="volver" href="menu.jsp">Volver</a>
    </center>    
    </body>
</html>
